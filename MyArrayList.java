package Project;

import java.util.*;
import java.util.function.UnaryOperator;

public class MyArrayList<E> implements List<E> {

    public int size = 0;
    Object[] array = null;

    public MyArrayList() {
       //aqui crio um novo array
        array = new Object[ 0 ];
    }

    @Override
    public int size() {
        if ( array == null ) {
       // aqui se o array for vazio retorna vazio
            return 0;
        } else {
            // ou retorna o tamanho do array
            return array.length;
        }
    }

    @Override
    public boolean isEmpty() {
        // se o tamanho do array esta vazio digo que é zero
        return array.length == 0;
    }

    @Override
    public boolean contains(Object o) {
        int i = 0;
        for (i = 0; i < array.length; i++) {

            if ( array[ i ].equals( o ) ) {
                return true;
            }
        }
        return false;
    }


    @Override
    public Iterator<E> iterator() {
        //DMI
        return null;
    }

    @Override
    public Object[] toArray() {
        //DMI for now
        return new Object[ 0 ];
    }

    @Override
    public <T> T[] toArray(T[] a) {
        //DMI for now
        return null;
    }

    @Override
    public boolean add(E e) {
        //adiciona uma nova casa ao array
        size = size + 1;
        //crio um novo array
        Object[] newArray = new Object[ size ];
        // aqui faco um loop para igualar o antigo array ao novo
        for (int i = 0; i < array.length; i++) {
            newArray[ i ] = array[ i ];
        }

        newArray[ size - 1 ] = e;
        array = newArray;
        return true;
    }

    @Override
    public boolean remove(Object o) {
        Object[] newArray = new Object[ size ];
        if ( size < 0 || o.equals( size ) ) {
          //caso o array esteja vazio atira uma excecao
            throw new IndexOutOfBoundsException();
        }
        //estou a criar um novo array ja com o elemento desejado removido
        o = newArray[ size ];
        for (int i = size; i < size - 1; i++) {
            newArray[ i ] = newArray[ i + 1 ];
        }
        size--;
        return true;
    }

    @Override
    public E remove(int index) {
        if ( index < 0 || index >= size ) {
            throw new IndexOutOfBoundsException( "Index: " + index + ", Size " + index );
        }
        E removedElement = (E) array[ index ];
        Object[] newArray = new Object[ size - 1 ];

        for (int i = 0; i < size - 1; i++) {
            if ( i >= index ) {
                newArray[ i ] = array[ i + 1 ];
            } else {
                newArray[ i ] = array[ i ];
            }
        }
        array = newArray;
        return removedElement;
    }

    @Override
    public boolean containsAll(Collection<?> c) {
        return false;
    }

    @Override
    public boolean addAll(Collection<? extends E> c) {
        return false;
    }

    @Override
    public boolean addAll(int index, Collection<? extends E> c) {
        return false;
    }

    @Override
    public boolean removeAll(Collection<?> c) {
        return false;
    }

    @Override
    public boolean retainAll(Collection<?> c) {
        //DMI
        return false;
    }

    @Override
    public void replaceAll(UnaryOperator<E> operator) {

    }

    @Override
    public void sort(Comparator<? super E> c) {

    }

    @Override
    public void clear() {
       //igualo o array a null
        array = null;
    }

    @Override
    public E get(int index) {
        if ( index < 0 || index >= size ) {
            throw new IndexOutOfBoundsException( "Index: " + index + ", Size " + index );
        }
        return (E) array[ index ]; // return value on index.
    }


    @Override
    public E set(int index, E element) {
        if ( index < 0 || index >= size ) {
            throw new IndexOutOfBoundsException( "Index: " + index + ", Size " + index );
        }
        if ( element == null ) {
            throw new NullPointerException();
        }

        return null;
    }

    @Override
    public void add(int index, E element) {
        size = size + 1;
        //crio um novo array
        Object[] newArray = new Object[ size ];

        //um loop para igualar as posicoes do index
        for (int i = 0; i < array.length; i++) {
            newArray[ i ] = array[ i ];
        }

        newArray[ size - 1 ] = element;
        // igualo o array antigo ao novo com mais um novo espaco que é o elemento adicionado
        array = newArray;

    }

    @Override
    public int indexOf(Object o) {
        return 0;
    }

    @Override
    public int lastIndexOf(Object o) {
        return 0;
    }

    @Override
    public ListIterator<E> listIterator() {
        //DMI
        return null;
    }

    @Override
    public ListIterator<E> listIterator(int index) {
        //DMI
        return null;
    }

    @Override
    public List<E> subList(int fromIndex, int toIndex) {
        //DMI for now
        return null;
    }

    @Override
    public Spliterator<E> spliterator() {
        return null;
    }
}
